import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Book} from "../Book/Book";
import {Observable} from "rxjs";
import {User} from "./User";
import { environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private url = environment.apiURL + 'user';
  constructor(private http: HttpClient) { }

  RegisterUser(user: User): Observable<User> {
    return this.http.post<User>(this.url + '/register', user);
  }
  GetUser(id: string): Observable<User>{
    const newurl = this.url + '/' + id;
    return  this.http.get<User>(newurl);
  }
}
