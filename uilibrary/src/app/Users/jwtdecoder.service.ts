import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class JwtdecoderService {
  constructor() { }

  public getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  public getRole(token: string): string {
    try {
      return this.getDecodedAccessToken(token).role;
    }
    catch (Error) {
      return null;
    }
  }

  public getId(token: string): string {
    try {
      return this.getDecodedAccessToken(token).unique_name;
    }
    catch (Error) {
      return null;
    }
  }
}
