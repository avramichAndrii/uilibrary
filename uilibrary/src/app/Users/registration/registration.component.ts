import { Component, OnInit } from '@angular/core';
import {AuthorService} from "../../Author/author.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../User";
import {UserService} from '../user.service'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  User = new User();
  private base64textString: string = '';
  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) { }


  addUser(){
    this.User.role = 'User';
    console.log(this.User);
    this.userService.RegisterUser(this.User).subscribe((data) => {
      console.log(data);
      this.router.navigate(['']);
    });
  }

  ngOnInit(): void {
  }
  handleFileSelect(evt){
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.User.imageData = this.base64textString;

    }
  }
  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.User.imageData = this.base64textString;
  }
}
