import { Component, OnInit } from '@angular/core';
import {User} from "../User";
import {UserService} from "../user.service";
import {JwtdecoderService} from "../jwtdecoder.service";
import {Router} from "@angular/router";
import {Book} from "../../Book/Book";
import {BookService} from "../../Book/book.service";
import {AuthorService} from "../../Author/author.service";
import {Author} from "../../Author/Author";

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.css']
})
export class CabinetComponent implements OnInit {
  User = new User();
  Books: Book[];
  Authors: Author[];
  Image: string;
  toggler: string;
  constructor(private userService: UserService,
              private jwtDecoderService: JwtdecoderService,
              private bookService: BookService,
              private authorService: AuthorService,
              private router: Router) { }

  ngOnInit(): void {
    this.toggler = 'Books';
    let id = this.jwtDecoderService.getId(localStorage.getItem('token'));
    if (id == null){
      this.router.navigate(['']);
    }
    this.userService.GetUser(id).subscribe((data) => {
      this.User = data;
      this.User.imageName = "data:image/png;base64," + data.imageData;
    });
    this.bookService.getAllBooks().subscribe((data) => {
      this.Books = data.bookCollection;
      this.Books.forEach(x => {
        this.authorService.getAuthorById(x.authorId).subscribe((data2) => {
          x.authorName = data2.name + ' ' + data2.lastName;
        });
      });
    });
  }
  AddAuthor(){
    this.router.navigate(['author/add']);
  }
 AddBook(){
   this.router.navigate(['Book/add']);
 }
 showBook(id: string){
    let route = 'Book/' + id;
    this.router.navigate([route]);
 }

  showAuthor(id: string){
    let route = 'author/' + id;
    this.router.navigate([route]);
  }

  update(id: string){
    let route = 'Book/update/' + id;
    this.router.navigate([route]);
  }

  delete(id: string){
    this.bookService.deleteBook(id).subscribe((data) => {
      let book = this.Books.find(com => com.id === id);
      let index = this.Books.indexOf(book);
      if (index > -1) {
        this.Books.splice(index, 1);
      }
    });
  }

  deleteAuthor(id: string){
    console.log(id);
    this.authorService.deleteAuthor(id).subscribe((data) => {
      let author = this.Authors.find(com => com.id === id);
      let index = this.Authors.indexOf(author);
      if (index > -1) {
        this.Authors.splice(index, 1);
      }
    });
  }

ShowAuthors(){
    this.authorService.getAllAuthors().subscribe((data) => {
      this.Authors = data.authorCollection;
    });
    if (this.toggler === 'Books'){
      this.toggler = 'Author';
    }else{
      this.toggler = 'Books';
    }
}
}
