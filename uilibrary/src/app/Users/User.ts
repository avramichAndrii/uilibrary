export class User {
  userId: string;
  login: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  role: string;
  imageData: any;
  imageName: string;
}
