import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Tokens} from "../Tokens";
import {Creedentials} from "../registration/Creedentials";
import  { environment } from "../../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
redirect: boolean;
toggle: number;
  constructor(private http: HttpClient,
              private router: Router) {
  }

  Creedential = new Creedentials();
  ngOnInit(): void {
    if (localStorage.getItem('token') != null ){
      this.router.navigate(['']);
    }
  }

  login() {
    const login = this.Creedential.Login;
    const password = this.Creedential.Password;
    return this.http.post<Tokens>(environment.apiURL + 'user/authenticate', {login, password})
      .subscribe(( data) => {

          const accessToken = data.accessToken;
          const refreshToken = data.refreshToken;
          const expiresAt = data.expiresAt;


          localStorage.setItem('token', JSON.stringify(accessToken));
          localStorage.setItem('refreshToken', JSON.stringify(refreshToken));
          localStorage.setItem('expiresAt', JSON.stringify(expiresAt));
          window.location.reload();

        }, status => {
          if (status === 'OK') {
            this.router.navigate(['/']);
            console.log('error post login');
          }
        }
      );

  }
  Register(){
    this.router.navigate(['user/register']);
  }
}

