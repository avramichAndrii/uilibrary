import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
 url = 'http://localhost:5001/comments';
  constructor(private http: HttpClient) { }

  getAllCommentsByBook(id: string): Observable<any> {
    const url1 = this.url + "/" + id;
    return this.http.get<any>(url1);
  }

  AddComment(comment: any): Observable<Comment>
  {
   return this.http.post<Comment>(this.url, comment);
  }

  DeleteComment(id: string): Observable<any> {
    const url1 = this.url + "/" + id;
    return this.http.delete<any>(url1);
  }
}
