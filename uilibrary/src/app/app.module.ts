import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './Book/books/book.component';
import { AddBookComponent } from './Book/add-book/add-book.component';
import {FormsModule} from "@angular/forms";
import { UpdatebookComponent } from './Book/updatebook/updatebook.component';
import { BookInfoComponent } from './Book/book-info/book-info.component';
import { AuthorInfoComponent } from './Author/author-info/author-info.component';
import { AuthorAddComponent } from './Author/author-add/author-add.component';
import { RegistrationComponent } from './Users/registration/registration.component';
import { LoginComponent } from './Users/login/login.component';
import { CabinetComponent } from './Users/cabinet/cabinet.component';
import {NavigationComponent} from "./navigation/navigation.component";
import { FooterComponent } from './navigation/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    AddBookComponent,
    UpdatebookComponent,
    BookInfoComponent,
    AuthorInfoComponent,
    AuthorAddComponent,
    RegistrationComponent,
    LoginComponent,
    CabinetComponent,
    NavigationComponent,
    NavigationComponent,
    FooterComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
