import { Component, OnInit } from '@angular/core';
import {Author} from "../Author";
import {AuthorService} from "../author.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-author-info',
  templateUrl: './author-info.component.html',
  styleUrls: ['./author-info.component.css']
})
export class AuthorInfoComponent implements OnInit {
  Author = new Author();

  constructor(
    private authorService: AuthorService,
    private router: Router,
    private route: ActivatedRoute) {
    let id = this.route.snapshot.paramMap.get('id');
    this.authorService.getAuthorById(id).subscribe((auth) => {
      this.Author = auth;
      this.Author.imageName = "data:image/png;base64," + auth.imageData;
    });
  }

  ngOnInit(): void {
  }

  GoToBook(id: string){
    let route: string = 'Book/' + id.toString();
    this.router.navigate([route]);
  }
}

