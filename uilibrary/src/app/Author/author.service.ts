import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Book} from "../Book/Book";
import {Author} from "./Author";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  private url =  environment.apiURL + 'author';
  constructor(private http: HttpClient) { }

  fixToken(){
    const token = localStorage.getItem('token');
    if (token != null){
      return token.toString().slice(1, token.length - 1);
    }
    return '';
  }


  getAllAuthors(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  getAuthorById(id: string): Observable<Author> {
    const url1 = this.url + "/" + id;
    return this.http.get<any>(url1);
  }

  addAuthor(author: Author): Observable<Author> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    return this.http.post<Author>(this.url, author, httpOptions);
  }

  deleteAuthor(id: string): Observable<Author> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    const url2 = this.url + "/" + id;
    return this.http.delete<any>(url2, httpOptions);
  }

  updateBook(id: string, book: Book): Observable<any> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    const url = `${this.url}/${id}`;
    return this.http.put(url, book, httpOptions);
  }
}

