import { Component, OnInit } from '@angular/core';
import {Author} from "../Author";
import {AuthorService} from "../author.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-author-add',
  templateUrl: './author-add.component.html',
  styleUrls: ['./author-add.component.css']
})
export class AuthorAddComponent implements OnInit {
Author = new Author();
  private base64textString: String = '';
  constructor(private authorService: AuthorService,
              private router: Router,
              private route: ActivatedRoute) { }


  addAuthor(){
    this.authorService.addAuthor(this.Author).subscribe((data) => {
      console.log(data);
      this.router.navigate(['']);
    });
  }

  ngOnInit(): void {
  }
  handleFileSelect(evt){
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.Author.imageData = this.base64textString;

    }
  }
  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.Author.imageData = this.base64textString;
  }
}
