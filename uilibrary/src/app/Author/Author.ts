import {Book} from "../Book/Book";

export class Author {
  public id: string;
  public name: string;
  public lastName: string;
  public year: number;
  public description: string;
  public books: Book[];

  public imageData: any;
  public imageName: string;
}
