import { Component, OnInit } from '@angular/core';
import {Book} from '../Book';
import {BookService} from '../book.service';
import {Author} from '../../Author/Author';
import {AuthorService} from '../../Author/author.service';
import {Router} from '@angular/router';
import {DataBookService} from '../data-book.service';
import {BookData} from '../BookData';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
BookToAdd = new Book();
BookData = new BookData();
Authors: Author[];
private base64textString: String = '';
private base64textString2: String = '';
  constructor(private bookService: BookService,
              private authorService: AuthorService,
              private router: Router,
              private dataBookService: DataBookService
              ) { }


  postBook(){
    console.log(this.BookToAdd);
    this.bookService.postBook(this.BookToAdd).subscribe((data) => {
      this.BookData.bookId = data.id;
      this.dataBookService.postBookData(this.BookData).subscribe((dataBinary) => {
        console.log(this.BookData);
      });
      this.router.navigate(['']);
    });
  }


  ngOnInit(): void {
    this.authorService.getAllAuthors().subscribe((data) => {
      this.Authors = data.authorCollection;
      console.log(data);
    });
  }


  handleBookDataSelect(evt) {
    const files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();
      reader.onload = this._handleReaderLoaded2.bind(this);
      reader.readAsBinaryString(file);
      this.BookData.data = this.base64textString2;

    }
  }
    _handleReaderLoaded2(readerEvt) {
      let binaryString = readerEvt.target.result;
      this.base64textString2 = btoa(binaryString);
      this.BookData.data = this.base64textString2;
    }

  handleFileSelect(evt){
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.BookToAdd.imageData = this.base64textString;

    }
  }
  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.BookToAdd.imageData = this.base64textString;
  }



}

