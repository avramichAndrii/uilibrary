import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Book} from "./Book";
import {Observable} from "rxjs";
import {BookData} from "./BookData";

@Injectable({
  providedIn: 'root'
})
export class DataBookService {

  private url = 'http://localhost:5001/storedBook';
  constructor(private http: HttpClient) { }

  postBookData(book: BookData): Observable<BookData> {
    return this.http.post<BookData>(this.url, book);
  }

}
