import { Component, OnInit } from '@angular/core';
import {Book} from "../Book";
import {BookService} from "../book.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-updatebook',
  templateUrl: './updatebook.component.html',
  styleUrls: ['./updatebook.component.css']
})
export class UpdatebookComponent implements OnInit {
BookToUpdate = new Book();
  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private router: Router
  ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.bookService.getBookById(id).subscribe((data) => {
      this.BookToUpdate = data;
    });
  }

  updateData() {
    this.bookService.updateBook(this.BookToUpdate.id, this.BookToUpdate).subscribe((data) => {

      this.router.navigate(['']);
    });
  }


}
