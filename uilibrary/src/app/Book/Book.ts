export class Book {
  public id: string;
  public name: string;
  public language: string;
  public year: number;
  public description: string;
  public dateOfPublishing: string;

  public authorId: string;
  public imageName: string;
  public imageData: any;




  public authorName: string;
}
