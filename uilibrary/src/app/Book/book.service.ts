import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Book} from "./Book";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private url = environment.apiURL + 'book';
  constructor(private http: HttpClient) { }


  fixToken(){
    const token = localStorage.getItem('token');
    if (token != null){
      return token.toString().slice(1, token.length - 1);
    }
    return '';
  }

  getAllBooks(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  getBookById(id: string): Observable<Book> {
    const url1 = this.url + "/" + id;
    return this.http.get<Book>(url1);
  }

  getBookByName(name: string): Observable<Book>{
    const nameURL = this.url + '/name/' + name;
    return  this.http.get<Book>(nameURL);
  }

  postBook(book: Book): Observable<Book> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    return this.http.post<Book>(this.url, book, httpOptions);
  }

  deleteBook(id: string): Observable<Book> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    const url2 = this.url + "/" + id;
    return this.http.delete<any>(url2, httpOptions);
  }

  updateBook(id: string, book: Book): Observable<any> {
    const httpOptions = {headers: new HttpHeaders({ Authorization: `Bearer ${this.fixToken()}`})};
    const url = `${this.url}/${id}`;
    return this.http.put(url, book, httpOptions);
  }
}
