import { Component, OnInit } from '@angular/core';
import {Book} from "../Book";
import {BookService} from "../book.service";
import {Router} from "@angular/router";
import {AuthorService} from "../../Author/author.service";

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
 Books: Book[];

  constructor(private bookService: BookService,
              private  router: Router,
              private authorService: AuthorService
              ) { }

  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe((data) => {
      console.log(data);
      this.Books = data.bookCollection;
      this.getImageName();
      this.Books.forEach(x => {
        this.authorService.getAuthorById(x.authorId).subscribe((data2) => {
          x.authorName = data2.name + ' ' + data2.lastName;
        });
      });
    });

  }

 AddAuthor(){
   this.router.navigate(['author/add']);
 }
  onSelect(book: Book){
    this.router.navigate(['Book', book.id]);
  }
  private getImageName(): void {
    this.Books.forEach(book => {
      book.imageName = "data:image/png;base64," + book.imageData;
    });
  }
}
