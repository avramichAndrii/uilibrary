import { Component, OnInit } from '@angular/core';
import {Book} from "../Book";
import {BookService} from "../book.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthorService} from "../../Author/author.service";
import { Comment } from '../../Comments/Comment';
import {CommentService} from "../../Comments/comment.service";
import {JwtdecoderService} from "../../Users/jwtdecoder.service";
import {UserService} from "../../Users/user.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.css']
})
export class BookInfoComponent implements OnInit {
Book = new Book();
Author: string;
Comments: Comment[];
CommentToAdd = new Comment();
UserId: string;
environment = environment.apiURL;
  constructor(private bookService: BookService,
              private authorService: AuthorService,
              private router: Router,
              private route: ActivatedRoute,
              private commentService: CommentService,
              private userService: UserService,
              private jwtDecoderService: JwtdecoderService
              ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');

      this.bookService.getBookById(id).subscribe((data) => {
        this.Book = data;
        this.Book.imageName = "data:image/png;base64," + data.imageData;
        this.authorService.getAuthorById(data.authorId).subscribe((auth) => {
          this.Author = auth.name + ' ' + auth.lastName;
        });
      });
      this.commentService.getAllCommentsByBook(id).subscribe((data) => {
        this.Comments = data.commentCollection;
        this.Comments.forEach(x => {
          this.userService.GetUser(x.userId).subscribe((data2) => {
            x.Name = data2.firstName + ' ' + data2.lastName;
          });
        });
      });
      let token = JSON.parse(localStorage.getItem('token'));
      if (token != null && token != undefined){
        this.UserId =  this.jwtDecoderService.getId(token.toString());
      }


  }

  postComment(){
    this.CommentToAdd.bookId = this.Book.id;
    this.CommentToAdd.userId = this.UserId;
    this.commentService.AddComment(this.CommentToAdd).subscribe((data) => {
     this.Comments.push(this.CommentToAdd);
    });
    window.location.reload();
  }

  showAuthor(){
    this.router.navigate(['/author', this.Book.authorId]);
  }


  deleteComment(id: string){
    let comment = this.Comments.find(com => com.commentId === id);
    let index = this.Comments.indexOf(comment);
    if (index > -1) {
      this.Comments.splice(index, 1);
    }
      this.commentService.DeleteComment(id).subscribe((data) => {
        this.Comments.splice(index, 1);
      });
  }


}
