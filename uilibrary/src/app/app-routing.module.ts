import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookComponent} from "./Book/books/book.component";
import {AddBookComponent} from "../app/Book/add-book/add-book.component";
import {BookInfoComponent} from "./Book/book-info/book-info.component";
import {UpdatebookComponent} from "./Book/updatebook/updatebook.component";
import {AuthorInfoComponent} from "./Author/author-info/author-info.component";
import {AuthorAddComponent} from "./Author/author-add/author-add.component";
import {RegistrationComponent} from "./Users/registration/registration.component";
import {LoginComponent} from "./Users/login/login.component";
import {CabinetComponent} from "./Users/cabinet/cabinet.component";


const routes: Routes = [
  { path: '', component: BookComponent},
  { path: 'Book/add', component: AddBookComponent},
  { path: 'Book/:id', component: BookInfoComponent },
  {path: 'Book/update/:id', component: UpdatebookComponent },
  {path: 'author/add', component: AuthorAddComponent },
  {path: 'author/:id', component: AuthorInfoComponent },
  {path: 'user/register', component: RegistrationComponent },
  {path: 'user/cabinet', component: CabinetComponent },
  {path: 'user/login', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
