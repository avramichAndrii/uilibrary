import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {BookService} from "../Book/book.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  tkn = localStorage.getItem('token');
  BookName: string;
  constructor(private router: Router,
              private bookService: BookService)
  {}

  SearchBookName(){
    try {
      this.bookService.getBookByName(this.BookName).subscribe((data) => {
        let route = 'Book/' + data.id;
        this.router.navigate([route]);
      });
    }
    catch (e) {
      alert('not found');
    }
  }

  ngOnInit(): void {
  }

  logOut(){
    localStorage.clear();
    window.location.reload();
    this.router.navigate(['']);
  }

}
